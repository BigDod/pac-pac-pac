import cocos
from cocos.actions.interval_actions import MoveTo, MoveBy, RotateBy
from math import sin, cos, pi
import pyglet

class Level1(cocos.scene.Scene):
    def __init__(self):
        super().__init__()
        Background=cocos.layer.Layer()
        Man = cocos.sprite.Sprite('man.png', position=(300,150), scale=0.06)
        Player =cocos.layer.Layer()
        Labirint = cocos.sprite.Sprite('level2.png', position=(300,250))
        Labirint.opacity = 150
        self.add(Background)
        self.add(Player)
        Background.add(Labirint)
        Player.add(Man)

        def update(dt):
            Man.x += dt*100

        Man.schedule_interval(update, 0.05)

        Map = [0,0,1,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,1,0,0,
               0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,
               0,0,1,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,1,0,0,
               0,0,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,0,0,
               0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,
               0,0,0,0,0,1,0,1,1,1,1,1,1,1,0,1,0,0,0,0,0,
               0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,
               0,1,1,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,1,1,0,
               0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,
               0,0,0,0,0,1,0,1,1,1,1,1,1,1,0,1,0,0,0,0,0,
               0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,
               0,0,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,0,0,
               0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,
               0,0,1,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,1,0,0]




cocos.director.director.init()
cocos.director.director.run(Level1())


        
